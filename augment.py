import numpy as np
from scipy import stats
import pandas as pd
from datetime import datetime, date, timedelta

def augment(inputDataframe, measures_at_minute, days, incremental_factor, aging_probability):
    
    data = inputDataframe
    std = np.std(data.ENV_TEMP, ddof=1)
    mean = np.mean(data.ENV_TEMP)
    augment_distribution = stats.norm(
        loc=mean, 
        scale=std
    )
    
    last_real = pd.to_datetime( data.tail(1).Timestamp )
    datetime_next_day = last_real + pd.DateOffset(days=1)
    datetime_next_day = datetime_next_day.dt.normalize() + pd.Timedelta('00:00:00')
    
    p = [(1 - aging_probability) / (len(incremental_factor) - 1)] * len(incremental_factor)
    p[0] = aging_probability
    
    for i in range(0, days):
        p = [(1 - aging_probability) / (len(incremental_factor) - 1)] * len(incremental_factor)
        p[int(i * len(incremental_factor) / days)] = aging_probability
        for j in range(0, 24):
            for h in range(0, 60, measures_at_minute):
                r = np.random.choice(incremental_factor, p=p)
                sample = augment_distribution.rvs() + r
                new_row = pd.DataFrame({'Timestamp':datetime_next_day,
            'ENV_HUMIDITY':0, 
            'ENV_TEMP':sample, 
            'TR_TEMP':0})
                data = pd.concat([data, new_row])
                datetime_next_day = datetime_next_day + pd.Timedelta('00:'+str(measures_at_minute)+':00')

    data = data.reset_index(drop=True)
    
    return data
    

def augmentFilELSX(inputFile, outputFile):
    data = pd.read_excel(inputFile)
    data = augment(data, 15, 365 * 2, [1, 2, 3, 5, 8, 13], 0.8)
    data.to_csv(outputFile)
    
def augmentFileCSV(inputFile, outputFile):
    data = pd.read_csv(inputFile)
    data = augment(data, 15, 365 * 2, [1, 2, 3, 5, 8, 13], 0.8)
    data.to_csv(outputFile)
    

#augmentFilELSX("data/data.xlsx", "data/data.csv")
    