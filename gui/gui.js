
function reset() {
    console.log("reset");
    $('#result').empty();
    $('#text').empty();
}

const csvmaker = function (headers, values) {
  
    // Empty array for storing the values
    csvRows = [];
  
    csvRows.push(headers.join(','));
  
    for (row of values){
      csvRows.push(row.join(','))
    }
    
    // Returning the array joining with new line 
    return csvRows.join('\n')
}

$(document).ready(function() {
    reset();
  
    $('#send').click(function() {
      let text = $("#text").val();
      send_text(text);
    })
  
    //setTimeout(get_gui_updates, 1);
  
});

$(document).ready(function() {
  
    document.getElementById("uploadButton_audio").onclick = () => {
      $('.image').empty();
      $('#agingPredictionButtonDiv').empty();
      $('#powerPredictionButtonDiv').empty();
      
      console.log("uploadButton clicked");
      let fileElement = document.getElementById('fileInput_audio')

      // check if user had selected a file
      if (fileElement.files.length === 0) {
        alert('please choose a file')
        return
      }
      
      days = $("#days").val();
      minutes_interval = $("#minutes_interval").val();
      probability = $("#probability").val();

      console.log(days);
      console.log(minutes_interval);
      console.log(probability);

      let params = {"days": days, "minutes_interval": minutes_interval, "probability": probability}

      axios.post("/define-params", params).then(res => {

        let file = fileElement.files[0];
    
        let formData = new FormData();
        formData.set('file', file);
        
        $('#spinner .spinner-border').show();

        axios.post("/upload-file-audio", formData, {
          onUploadProgress: progressEvent => {
            const percentCompleted = Math.round(
              (progressEvent.loaded * 100) / progressEvent.total
            );
            console.log(`upload process: ${percentCompleted}%`);
          }
        })
          .then(res => {
            $('#spinner .spinner-border').hide();
            console.log(res.data)
            const blob = new Blob([res.data], { type: 'text/csv' }); 
    
            // Creating an object for downloading url 
            const url = window.URL.createObjectURL(blob) 
          
            // Creating an anchor(a) tag of HTML 
            const a = document.createElement('a') 
          
            // Passing the blob downloading url  
            a.setAttribute('href', url) 
          
            // Setting the anchor tag attribute for downloading 
            // and passing the download file name 
            a.setAttribute('download', 'augmented_file.csv'); 
          
            // Performing a download with click 
            a.click() 
          })

      });

      
    };
  
  });