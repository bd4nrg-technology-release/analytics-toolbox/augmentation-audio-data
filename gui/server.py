import os
import logging
import json
import time
import fastapi
import fastapi.templating
from fastapi.staticfiles import StaticFiles
from fastapi import File, UploadFile
from fastapi.responses import FileResponse
import grpc
import concurrent.futures

import traceback
import queue
import csv
import codecs
import PIL.Image as Image
import io

from pydantic import BaseModel
from typing import List, Optional
import pydantic

import augment_gui_pb2
import augment_gui_pb2_grpc

import augment

logger  = logging.getLogger(__name__)
# the next line sets logging level for things outside uvicorn, that means for the gRPC server
# set Python logging level for uvicorn when starting uvicorn!
logging.basicConfig(level=logging.INFO)


class AUGMENTGUIServicer(augment_gui_pb2_grpc.AUGMENTGUIServicer):
    def processAudioData(self, request, context):
        return augment_gui_pb2.Empty()


app = fastapi.FastAPI(title='AUGMENT_GUI', debug=True)
app.logger = logger
app.mount("/imgs", StaticFiles(directory="imgs"), name='images')

configfile = os.environ['CONFIG'] if 'CONFIG' in os.environ else "config.json"
logging.info("loading config from %s", configfile)
config = json.load(open(configfile, 'rt'))
audio_js_to_protobuf_queue = queue.Queue()
audioAugmented_protobuf_to_js_queue = queue.Queue()

templates = fastapi.templating.Jinja2Templates(directory='templates')

grpcserver = grpc.server(concurrent.futures.ThreadPoolExecutor(max_workers=10))
augment_gui_pb2_grpc.add_AUGMENTGUIServicer_to_server(AUGMENTGUIServicer(), grpcserver)
grpcport = config['grpcport']
# listen on all interfaces (otherwise docker cannot export)
grpcserver.add_insecure_port('0.0.0.0:'+str(grpcport))
logging.info("starting grpc server at port %d", grpcport)
grpcserver.start()

class Params(BaseModel):
    days: int
    minutes_interval: int
    probability: float

days = 365
minutes_interval = 15
probability = 0.8
        
@app.get('/')
def serve_website(request: fastapi.Request):
    return templates.TemplateResponse("gui.html", { 'request': request }, headers={'Cache-Control': 'no-cache'})

@app.get('/gui.js')
def serve_js(request: fastapi.Request):
    return fastapi.responses.FileResponse("gui.js", headers={'Cache-Control': 'no-cache'})

@app.get('/gui.css')
def serve_css(request: fastapi.Request):
    return fastapi.responses.FileResponse("gui.css", headers={'Cache-Control': 'no-cache'})

@app.post('/define-params', response_model=None)
def defineParams(params: Params):
    print(params)
    global days
    global minutes_interval
    global probability
    days = params.days
    minutes_interval = params.minutes_interval
    probability = params.probability
    return
    
@app.post('/upload-file-audio', response_model=None)
def uploadFileAudio(file: UploadFile):
    
    global days
    global minutes_interval
    global probability
    
    print("inside upload-file POST function")
    logging.info("inside uploadFileAudio POST function")
    
    print(file.filename)
    
    csvReader = csv.DictReader(codecs.iterdecode(file.file, 'utf-8'))
    csvReaderList = list(csvReader)
    
    with open('input.csv', 'w', newline='') as csvfile:
        #fieldnames = ['ts', 'maxDb', 'casted_timestamp']
        fieldnames = ['ts','maxDb','casted_timestamp']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        
        for row in csvReaderList:
            writer.writerow({'ts': row['ts'], 'maxDb': row['maxDb'], 'casted_timestamp': row['casted_timestamp']})
    
    augment.augmentFileCSV('input.csv', 'output.csv', minutes_interval, days, probability)
    
    file = open("output.csv", "r")
    data = list(csv.reader(file, delimiter=","))
    file.close()
    
    response = FileResponse("output.csv", media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=augmented_file.csv"
    
    return response