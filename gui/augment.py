import numpy as np
from scipy import stats
import pandas as pd
from datetime import datetime, date, timedelta

def augment(inputDataframe, measures_at_minute, days, incremental_factor, aging_probability):
    
    data = inputDataframe
    print(data.head())
    print(data.dtypes)
    std = np.std(data.maxDb, ddof=1)
    mean = np.mean(data.maxDb)
    augment_distribution = stats.norm(
        loc=mean, 
        scale=std
    )
    
    last_real = pd.to_datetime( data.tail(1).ts )
    datetime_next_day = last_real + pd.DateOffset(days=1)
    datetime_next_day = datetime_next_day.dt.normalize() + pd.Timedelta('00:00:00')
    
    p = [(1 - aging_probability) / (len(incremental_factor) - 1)] * len(incremental_factor)
    p[0] = aging_probability
    
    for i in range(0, days):
        print("day " + str(i) + " of " + str(days))
        p = [(1 - aging_probability) / (len(incremental_factor) - 1)] * len(incremental_factor)
        p[int(i * len(incremental_factor) / days)] = aging_probability
        for j in range(0, 24):
            for h in range(0, 60, measures_at_minute):
                r = np.random.choice(incremental_factor, p=p)
                sample = augment_distribution.rvs() + r
                new_row = pd.DataFrame({
                    'ts': datetime_next_day,
                    'maxDb': sample, 
                    'casted_timestamp': 1
                })
                data = pd.concat([data, new_row])
                datetime_next_day = datetime_next_day + pd.Timedelta('00:'+str(measures_at_minute)+':00')

    data['casted_timestamp'] = pd.to_datetime(data['ts'],format= '%Y-%m-%d %H:%M:%S')
    data['casted_timestamp'] = np.floor(data.casted_timestamp.astype('int64') / 1000000000).astype(int)
    data = data.reset_index(drop=True)
    
    print('done')
    
    return data
    

def augmentFileELSX(inputFile, outputFile, measures_at_minute, days, aging_probability):
    data = pd.read_excel(inputFile)
    data = augment(data, measures_at_minute, days, [1, 2, 3, 5, 8, 13], aging_probability)
    data.to_csv(outputFile)
    
def augmentFileCSV(inputFile, outputFile, measures_at_minute, days, aging_probability):
    data = pd.read_csv(inputFile)
    data = augment(data, measures_at_minute, days, [1, 2, 3, 5, 8, 13], aging_probability)
    data.to_csv(outputFile)
    